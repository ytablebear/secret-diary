module https: //gitee.com/rocket049/secret-diary

go 1.18

require github.com/therecipe/qt v0.0.0-20200904063919-c0c124a5770d

require (
	github.com/mattn/go-sqlite3 v1.14.12
	github.com/rocket049/gettext-go v0.0.0-20190410112519-14a7c4752bef
	github.com/rocket049/go-locale v0.0.0-20190410112613-f30d0ad89b65
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	golang.org/x/text v0.3.0
)

require (
	github.com/gopherjs/gopherjs v0.0.0-20190411002643-bd77b112433e // indirect
	github.com/therecipe/qt/internal/binding/files/docs/5.13.0 v0.0.0-20200904063919-c0c124a5770d // indirect
)
